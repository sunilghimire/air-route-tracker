import turtle
from tkinter import *
from tkinter import messagebox


def execute_program():
    if departure.get().lower() == 'nepal' and arrival.get().lower() == 'mumbai':
        nepal_to_mumbai()
    elif departure.get().lower() == 'mumbai' and arrival.get().lower() == 'washington':
        mumbai_to_washington()
    elif departure.get().lower() == 'london' and arrival.get().lower() == 'france':
        london_to_france()
    elif departure.get().lower() == 'london' and arrival.get().lower() == 'mumbai':
        london_to_mumbai()
    else:
        messagebox.showwarning("Airplane Route", "Enter Valid Country Name")


def nepal_to_mumbai():
    root.destroy()
    screen = turtle.Screen()
    screen.setup(1357, 628)
    # you will use your image name with .gif format instead of map.gif
    screen.bgpic('map.gif')
    turtle.penup()
    turtle.setposition(-350, 196)
    turtle.right(4)
    turtle.pendown()
    for i in range(37):
        turtle.forward(16)
        turtle.right(0.7)

    screen.mainloop()


def mumbai_to_washington():
    root.destroy()
    screen = turtle.Screen()
    screen.setup(1357, 628)
    # you will use your image name with .gif format instead of map.gif
    screen.bgpic('map.gif')
    turtle.penup()
    turtle.setposition(196, 20)
    turtle.left(110)
    turtle.pendown()
    for i in range(15):
        turtle.forward(50)
        turtle.left(8.4)
    screen.mainloop()


def london_to_france():
    root.destroy()
    screen = turtle.Screen()
    screen.setup(1357, 628)
    # you will use your image name with .gif format instead of map.gif
    screen.bgpic('map.gif')
    turtle.penup()
    turtle.setposition(-107, 196)
    turtle.right(4)
    turtle.pendown()
    for i in range(45):
        turtle.forward(16)
        turtle.right(1.2)

    screen.mainloop()


def london_to_mumbai():
    root.destroy()
    screen = turtle.Screen()
    screen.setup(1357, 628)
    # you will use your image name with .gif format instead of map.gif
    screen.bgpic('map.gif')
    turtle.penup()
    turtle.setposition(-107, 196)
    turtle.right(4)
    turtle.pendown()
    for i in range(36):
        turtle.forward(10)
        turtle.right(1.4)
    screen.mainloop()


root = Tk()
root.title("PYTHON | AIRPLANE ROUTE ")
width = root.winfo_screenwidth()
height = root.winfo_screenheight()
x = int(width / 2 - 600 / 2)
y = int(height / 2 - 400 / 2)
str1 = "500x170+" + str(x) + "+" + str(y)
root.geometry(str1)
footer = Label(text="© 2020 Tech Tutor. All Right Reserved", bg="white")
footer.config(font=('Courier', 11, 'bold'))
footer.place(x=x - 270, y=y - 35)

# disable the resize of the window
root.resizable(width=False, height=False)
lab = Label(root, text='AIR ROUTE TRACKER', font=25)
dep = Label(root, text='Source:')
departure = Entry(root)
ar = Label(root, text='Destination:')
arrival = Entry(root)
button = Button(root, text='View Route', bg='Light Green', command=execute_program)

lab.pack()
dep.pack()
departure.pack()
ar.pack()
arrival.pack()
button.pack()
root.mainloop()
